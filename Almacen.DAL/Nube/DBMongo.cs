﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.DAL.Nube
{
    public class DBMongo<T> : IDbNoSql<T> where T : Base
    {

        string cadenaConexion = @"mongodb+srv://midscarlosespinoza:Ui7ZoypYOoau3mcz@almacenesmaui.8c9f1ly.mongodb.net/?retryWrites=true&w=majority";
        MongoClient mongoClient;
        IMongoDatabase db;
        BaseValidator<T> validador;

        protected IMongoCollection<T> Collection() => db.GetCollection<T>(typeof(T).Name);

        public DBMongo(BaseValidator<T> validador)
        {
            this.validador = validador;
            Error = "";
            var settings = MongoClientSettings.FromConnectionString(cadenaConexion);
            mongoClient = new MongoClient(settings);
            db = mongoClient.GetDatabase("AlmacenesMAUI");
        }

        public string Error { get; protected set; }

        public List<T> Read
        {
            get
            {
                try
                {
                    Error = "";
                    return Collection().AsQueryable().ToList();
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public T Create(T entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            entidad.FechaHora=DateTime.Now;
            var resultado = validador.Validate(entidad);
            if (resultado.IsValid)
            {
                try
                {
                    Error = "";
                    Collection().InsertOne(entidad);
                    return entidad;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
            else
            {
                Error = "";
                foreach (var item in resultado.Errors)
                {
                    Error += item.ErrorMessage + ". ";
                }
                return null;
            }

            
        }

        public bool Delete(T entidad)
        {
            try
            {
                Error = "";
                return Collection().DeleteOne(e => e.Id == entidad.Id).DeletedCount > 0;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public T GetById(string id)
        {
            try
            {
                Error = "";
                return Query(e => e.Id == id).SingleOrDefault();
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public List<T> Query(Expression<Func<T, bool>> predicado)
        {
            try
            {
                Error = "";
                return Collection().Find(predicado).ToList();
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public T Update(T entidad)
        {
            entidad.FechaHora = DateTime.Now;
            var resultado = validador.Validate(entidad);
            if (resultado.IsValid)
            {
                try
                {
                    Error = "";
                    return Collection().ReplaceOne(e => e.Id == entidad.Id, entidad).ModifiedCount == 1 ? entidad : null;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
            else
            {
                Error = "";
                foreach (var item in resultado.Errors)
                {
                    Error += item.ErrorMessage + ". ";
                }
                return null;
            }
        }
    }
}
