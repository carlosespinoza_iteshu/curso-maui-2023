﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.DAL.Local
{
    public class DbLiteDB<T> : IDbNoSql<T> where T : Base
    {
        private string RutaDB;
        BaseValidator<T> validador;
        public DbLiteDB(string rutaBD, BaseValidator<T> validador)
        {
            RutaDB = rutaBD;
            this.validador = validador;
        }
        public List<T> Read
        {
            get
            {
                try
                {
                    List<T> list = new List<T>();
                    using (var db = new LiteDatabase(RutaDB))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        list = col.FindAll().ToList();
                    }
                    Error = "";
                    return list;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public string Error { get; private set; }

        public T Create(T entidad)
        {
            try
            {
                entidad.Id = Guid.NewGuid().ToString();
                entidad.FechaHora=DateTime.Now;

                var resultadoValidacion = validador.Validate(entidad);

                if (resultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(RutaDB))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Insert(entidad);
                    }
                    Error = "";
                    return entidad;
                }
                else
                {
                    Error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        Error += item.ErrorMessage + ". ";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public bool Delete(T entidad)
        {
            try
            {
                bool r;
                using (var db = new LiteDatabase(RutaDB))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    r = col.Delete(entidad.Id);
                }
                Error = "";
                return r;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public T GetById(string id)
        {
            try
            {
                T item;
                using (var db = new LiteDatabase(RutaDB))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    item = col.Find(i => i.Id == id).SingleOrDefault();
                }
                Error = "";
                return item;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public List<T> Query(Expression<Func<T, bool>> predicado)
        {
            try
            {
                return Read.Where(predicado.Compile()).ToList();
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public T Update(T entidad)
        {
            try
            {
                bool r;
                entidad.FechaHora = DateTime.Now;
                var resultadoDeValidacion = validador.Validate(entidad);
                if (resultadoDeValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(RutaDB))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        r = col.Update(entidad);
                    }
                    Error = "";
                    return r ? entidad : null;
                }
                else
                {
                    Error = "";
                    foreach (var item in resultadoDeValidacion.Errors)
                    {
                        Error += item.ErrorMessage + ". ";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}
