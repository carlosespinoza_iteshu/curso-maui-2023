﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.DAL
{
    public interface IDbNoSql<T> where T : Base
    {
        //CRUD
        //Create -> Insert
        //Read   -> Select
        //Update -> Update
        //Delete -> Delete

        /// <summary>
        /// Obtiene el error (si es que existe) después de una operación
        /// </summary>
        string Error { get; }


        /// <summary>
        /// Obtiene todos los registros de la tabla
        /// </summary>
        List<T> Read { get; }
        /// <summary>
        /// Obtiene el registro relacionado al Id Proporcionado
        /// </summary>
        /// <param name="id">Id del elemento a obtener</param>
        /// <returns>Elemento cuyo Id sea el proporcionado</returns>
        T GetById(string id);
        /// <summary>
        /// Inserta un elemento en la tabla
        /// </summary>
        /// <param name="entidad">Elemento a insertar</param>
        /// <returns>Elemento insertado con Id</returns>
        T Create(T entidad);
        /// <summary>
        /// Actualiza un elemento de acuerdo al Id del mismo
        /// </summary>
        /// <param name="entidad">Elemento con los datos a actualizar</param>
        /// <returns>Elemento modificado</returns>
        T Update(T entidad);
        /// <summary>
        /// Elimina un elemento de la tabla
        /// </summary>
        /// <param name="entidad">Elemento a eliminar</param>
        /// <returns>Si se pudo eliminar o no</returns>
        bool Delete(T entidad);
        /// <summary>
        /// Ejecuta una consulta sobre la tabla
        /// </summary>
        /// <param name="predicado">Expresión lambda de la consulta</param>
        /// <returns>Resultado de la consulta.</returns>
        List<T> Query(Expression<Func<T, bool>> predicado);
    }
}
