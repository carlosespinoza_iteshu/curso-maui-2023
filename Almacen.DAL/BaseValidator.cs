﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.DAL
{
    public class BaseValidator<T>:AbstractValidator<T> where T : Base
    {
        public BaseValidator()
        {
            RuleFor(p => p.Id).NotEmpty();
            RuleFor(p => p.FechaHora).NotEmpty();
        }
    }
}
