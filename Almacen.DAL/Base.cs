﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.DAL
{
    public abstract class Base
    {
        public string Id { get; set; }
        public DateTime FechaHora { get; set; }
    }
}
