﻿namespace App01Basicos;

public partial class MainPage : ContentPage
{
    int count = 0;

    public MainPage()
    {
        InitializeComponent();
        List<string> generos = new List<string>();
        generos.Add("Hombre");
        generos.Add("Mujer");
        prkGenero.ItemsSource = generos;
    }

    private void btnGenerarCURP_Clicked(object sender, EventArgs e)
    {
        string curp = "";
        curp = entAPaterno.Text.Substring(0, 2);
        curp = curp + entAMaterno.Text.Substring(0, 1);
        curp += entNombre.Text.Substring(0, 1);
        if (entAnio.Text.Length < 3)
        {
            DisplayAlert("Error", "El año debe tener cuatro dígitos", "Ok");
        }
        else
        {
            DateTime fecha = new DateTime(int.Parse(entAnio.Text), int.Parse(entMes.Text), int.Parse(entDia.Text));
            curp += fecha.Year.ToString().Substring(2, 2);
            curp += Corregir(fecha.Month);
            curp += Corregir(fecha.Day);
            if(prkGenero.SelectedItem!= null)
            {
                curp += prkGenero.SelectedItem.ToString().Substring(0, 1);
                curp += entEstado.Text.Substring(0, 2);
                lblCURP.Text = curp.ToUpper();
            }
            else
            {
                DisplayAlert("Error", "Debe seleccionar un genero", "Ok");
            }
            
        }
    }

    private string Corregir(int numero)
    {
        if (numero < 10)
        {
            return "0" + numero.ToString();
        }
        else
        {
            return numero.ToString();
        }
    }
}

