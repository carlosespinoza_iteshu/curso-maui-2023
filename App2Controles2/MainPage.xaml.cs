﻿using App2Controles2.Models;
using App2Controles2.Pages;

namespace App2Controles2
{
    public partial class MainPage : ContentPage
    {
        Contacto contacto;
        List<Contacto> contactos;

        public MainPage(LoginModel model, string usuario)
        {
            InitializeComponent();
            contacto = new Contacto()
            {
                FechaNacimiento=DateTime.Now
            };
            contactos = new List<Contacto>();
            ActualizarLista();
            DisplayAlert("Bienvenido", $"{usuario}, recuerda que tu contraseña es {model.Password}", "Ok");
        }

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            contactos.Add(contacto);
            contacto = new Contacto()
            {
                FechaNacimiento = DateTime.Now
            };
            ActualizarLista();
        }

        private void ActualizarLista()
        {
            BindingContext = contacto;
            lstContactos.ItemsSource = null;
            lstContactos.ItemsSource = contactos;
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {
            Contacto c=lstContactos.SelectedItem as Contacto;
            if(c != null)
            {
                contactos.Remove(c);
                ActualizarLista();
            }
            else
            {
                DisplayAlert("Error", "Primero seleccione un contacto", "Ok");
            }
        }

        private void btnRegresar_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}