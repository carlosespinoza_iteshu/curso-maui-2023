using App2Controles2.Models;

namespace App2Controles2.Pages;

public partial class LoginPage : ContentPage
{
    LoginModel model;
    public LoginPage()
    {
        InitializeComponent();
        model = new LoginModel();
        BindingContext = model;
    }

    private void btnIniciarSesion_Clicked(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(model.Usuario))
        {
            if (!string.IsNullOrEmpty(model.Password))
            {
                if (model.Password == "1234567890")
                {
                    Navigation.PushAsync(new MainPage(model, model.Usuario));
                }
                else
                {
                    DisplayAlert("Error", "Contrase�a incorrecta", "Ok");
                }
            }
            else
            {
                DisplayAlert("Error", "La contrase�a no puede estar vac�a", "Ok");
            }
        }
        else
        {
            DisplayAlert("Error", "El usuario no puede estar vaci�", "Ok");
        }
    }
}