﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2Controles2
{
    internal class Contacto
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string ColorFavorito { get; set; }

        public override string ToString()
        {
            return $"{Nombre} {Apellidos}";
        }
    }
}
