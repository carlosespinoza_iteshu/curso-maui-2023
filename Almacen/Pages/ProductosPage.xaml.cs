using Almacen.DAL;
using Almacen.DAL.Local;
using Almacen.DAL.Nube;
using Almacen.Entidades;
using Almacen.Modelos;
using Almacen.Validadores;

namespace Almacen.Pages;

public partial class ProductosPage : ContentPage
{
    IDbNoSql<Producto> repositorio;
    IDbNoSql<Categoria> repositorioCategorias;
	public ProductosPage()
	{
		InitializeComponent();
        string path = Path.Combine(FileSystem.AppDataDirectory, "Almacen.db");
        //repositorio = new DbLiteDB<Producto>(path, new ProductoValidator());
        //repositorioCategorias=new DbLiteDB<Categoria>(path, new CategoriaValidator());
        repositorio=new DBMongo<Producto>(new ProductoValidator());
        repositorioCategorias = new DBMongo<Categoria>(new CategoriaValidator());
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        //List<Categoria> categorias = repositorioCategorias.Read;
        List<ProductoConCategoriaModel> elementos = new List<ProductoConCategoriaModel>();
        foreach (var item in repositorio.Read)
        {
            elementos.Add(new ProductoConCategoriaModel()
            {
                Descripcion = item.Descripcion,
                FechaHora = item.FechaHora,
                Id = item.Id,
                IdCategoria = item.IdCategoria,
                Nombre = item.Nombre,
                Precio = item.Precio,
                NombreCategoria = repositorioCategorias.GetById(item.IdCategoria).Nombre
            });
        }



        lstProductos.ItemsSource = null;
        lstProductos.ItemsSource = elementos;
    }

    private void lstProductos_ItemTapped(object sender, ItemTappedEventArgs e)
    {
        Navigation.PushAsync(new EditarProducto((e.Item as Producto), repositorio));
    }

    private void btnAgregarCategoria_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarProducto(new Producto(), repositorio));

    }
}