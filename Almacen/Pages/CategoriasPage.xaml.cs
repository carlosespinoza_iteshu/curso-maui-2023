using Almacen.DAL;
using Almacen.DAL.Local;
using Almacen.DAL.Nube;
using Almacen.Entidades;
using Almacen.Validadores;

namespace Almacen.Pages;

public partial class CategoriasPage : ContentPage
{
	IDbNoSql<Categoria> repositorioCategorias;
	public CategoriasPage()
	{
		InitializeComponent();
		string path = Path.Combine(FileSystem.AppDataDirectory, "Almacen.db");
		//repositorioCategorias = new DbLiteDB<Categoria>(path,new CategoriaValidator()); //Usando LiteDB
		repositorioCategorias = new DBMongo<Categoria>(new CategoriaValidator()); //Usando MongoDB

    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
		lstCategorias.ItemsSource = null;
		var datos= repositorioCategorias.Read;
		if( datos != null)
		{
			lstCategorias.ItemsSource = datos;
		}
		else
		{
			DisplayAlert("Error", repositorioCategorias.Error, "Ok");
		}
    }

    private void btnAgregarCategoria_Clicked(object sender, EventArgs e)
	{
		Navigation.PushAsync(new EditarCategoria(new Categoria(),repositorioCategorias));
	}

    private void lstCategorias_ItemTapped(object sender, ItemTappedEventArgs e)
    {
		Navigation.PushAsync(new EditarCategoria(e.Item as Categoria,repositorioCategorias));
    }
}