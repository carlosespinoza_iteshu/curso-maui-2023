using Almacen.DAL;
using Almacen.DAL.Local;
using Almacen.DAL.Nube;
using Almacen.Entidades;
using Almacen.Validadores;

namespace Almacen.Pages;

public partial class EditarProducto : ContentPage
{
	Producto producto;
	IDbNoSql<Producto> repositorio;
	IDbNoSql<Categoria> repositorioCategoria;
	bool esNuevo;
	public EditarProducto(Producto producto, IDbNoSql<Producto> repositorio)
	{
		InitializeComponent();
		this.producto = producto;
		this.repositorio = repositorio;
		BindingContext = producto;
		//repositorioCategoria = new DbLiteDB<Categoria>(Path.Combine(FileSystem.AppDataDirectory, "Almacen.db"),new CategoriaValidator());
		repositorioCategoria = new DBMongo<Categoria>(new CategoriaValidator());
		pkrCategoria.ItemsSource = repositorioCategoria.Read;
		if (string.IsNullOrEmpty(producto.Id))
		{
			esNuevo = true;
			Title = "Nuevo Producto";
			btnEliminar.IsVisible = false;
		}
		else
		{
			esNuevo=false;
			Title = "Editar Producto";
			pkrCategoria.SelectedItem=repositorioCategoria.GetById(producto.IdCategoria);
			btnEliminar.IsVisible = true;
		}
	}

	private void btnGuardar_Clicked(object sender, EventArgs e)
	{
		var categoria = (pkrCategoria.SelectedItem as Categoria);
		if(categoria != null )
		{
            producto.IdCategoria = categoria.Id;
            var r = esNuevo ? repositorio.Create(producto) : repositorio.Update(producto);
            if (r != null)
            {
                DisplayAlert("�xito", "Elemento guardado correctamente", "Ok");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error", repositorio.Error, "Ok");

            }
		}
		else
		{
			DisplayAlert("Error", "Primero selecciona una categor�a", "Ok");
		}
        
	}

	private void btnEliminar_Clicked(object sender, EventArgs e)
	{
		if (repositorio.Delete(producto))
		{
			DisplayAlert("�xito", "Elemento guardado correctamente", "Ok");
			Navigation.PopAsync();
		}
		else
		{
			DisplayAlert("Error", repositorio.Error, "Ok");

		}
	}
}