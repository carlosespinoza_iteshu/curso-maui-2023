using Almacen.DAL;
using Almacen.Entidades;

namespace Almacen.Pages;

public partial class EditarCategoria : ContentPage
{
    Categoria categoria;
    IDbNoSql<Categoria> repositorio;
    bool esNuevo;
    public EditarCategoria(Categoria categoria, IDbNoSql<Categoria> repositorio)
    {
        InitializeComponent();
        this.categoria = categoria;
        this.repositorio = repositorio;
        if (string.IsNullOrEmpty(categoria.Id))
        {
            //Nueva categoria
            Title = "Nueva categor�a";
            btnEliminar.IsVisible = false;
            esNuevo = true;
        }
        else
        {
            //Editando la categoria
            Title = "Editar categor�a";
            entNombre.Text = categoria.Nombre;
            btnEliminar.IsVisible = true;
            esNuevo = false;
        }
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        categoria.Nombre = entNombre.Text;
        Categoria r;
        if (esNuevo)
        {
            r = repositorio.Create(categoria);
        }
        else
        {
            r = repositorio.Update(categoria);
        }

        if (r != null)
        {
            DisplayAlert("�xito", "Elemento guardado correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", repositorio.Error, "Ok");
        }
    }

    private void btnEliminar_Clicked(object sender, EventArgs e)
    {
        if(repositorio.Delete(categoria))
        {
            DisplayAlert("�xito", "Elemento eliminado correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", repositorio.Error, "Ok");
        }
    }
}