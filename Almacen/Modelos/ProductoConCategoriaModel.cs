﻿using Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.Modelos
{
    public class ProductoConCategoriaModel:Producto
    {
        public string NombreCategoria { get; set; }
    }
}
