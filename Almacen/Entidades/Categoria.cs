﻿using Almacen.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.Entidades
{
    public class Categoria:Base
    {
        public string Nombre { get; set; }
        public override string ToString()
        {
            return Nombre;
        }
    }
}
