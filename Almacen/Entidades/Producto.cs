﻿using Almacen.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.Entidades
{
    public class Producto:Base
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string IdCategoria { get; set; }
        public float Precio { get; set; }
    }
}
