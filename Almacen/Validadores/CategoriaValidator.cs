﻿using Almacen.DAL;
using Almacen.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.Validadores
{
    public class CategoriaValidator:BaseValidator<Categoria>
    {
        public CategoriaValidator()
        {
           RuleFor(c => c.Nombre)
                .NotEmpty().WithMessage("Te falto el nombre")
                .MinimumLength(4).WithMessage("Pon mas de 4 letras");
        }
    }
}
