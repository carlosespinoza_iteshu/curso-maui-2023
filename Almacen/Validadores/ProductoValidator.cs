﻿using Almacen.DAL;
using Almacen.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Almacen.Validadores
{
    public class ProductoValidator:BaseValidator<Producto>
    {
        public ProductoValidator()
        {
            
            RuleFor(p=>p.IdCategoria).NotEmpty();
            RuleFor(p => p.Nombre).NotEmpty().MinimumLength(4);
            RuleFor(p=>p.Descripcion).NotEmpty().MinimumLength(6);
            RuleFor(p => p.Precio).GreaterThan(0);
        }
    }
}
